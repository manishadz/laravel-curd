<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
    <title>Document</title>
</head>
<body>
    <div class="container mt-5" >
        <div class="row">
            <div class="col-sm-6">
                <form action="" method="POST">
                    @csrf
                    @method('PUT')
                    <div class="mb-3">
                        <label for="name"class="form-lable">Name</label>
                        <input type="text" class="form-controller"id="name"name="name" value="{{$student->name}}">
                    </div>
                    <div class="mb-3">
                        <label for="city"class="form-lable">City</label>
                        <input type="text" class="form-controller"id="city"name="city" value="{{$student->city}}">
                    </div>
                    <div class="mb-3">
                        <label for="marks"class="form-lable">Marks</label>
                        <input type="number" class="form-controller"id="marks"name="marks" value="{{$student->marks}}">
                    </div>
                    <button type="submit" class="btn btn-primary">update</button>
                </form>
                @if (session()->has('status'))
                <div class="alert alert-success">
                {{session('status')}}
                </div>

                    @endif
            </div>
        </div>
    </div>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-kenU1KFdBIe4zVF0s0G1M5b4hcpxyD9F7jL+jjXkk+Q2h455rYXK/7HAuoJl+0I4" crossorigin="anonymous"></script>
</body>
</html>
